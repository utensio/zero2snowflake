
-- 3.1.4
create or replace table trips  
(tripduration integer,
  starttime timestamp,
  stoptime timestamp,
  start_station_id integer,
  start_station_name string,
  start_station_latitude float,
  start_station_longitude float,
  end_station_id integer,
  end_station_name string,
  end_station_latitude float,
  end_station_longitude float,
  bikeid integer,
  membership_type string,
  usertype string,
  birth_year integer,
  gender integer);

--create file file_format
CREATE FILE FORMAT "CITIBIKE"."PUBLIC".CSV 
TYPE = 'CSV' COMPRESSION = 'AUTO' FIELD_DELIMITER = ',' RECORD_DELIMITER = '\n' SKIP_HEADER = 0 FIELD_OPTIONALLY_ENCLOSED_BY = '\042' TRIM_SPACE = FALSE 
ERROR_ON_COLUMN_COUNT_MISMATCH = FALSE ESCAPE = 'NONE' ESCAPE_UNENCLOSED_FIELD = '\134' DATE_FORMAT = 'AUTO' TIMESTAMP_FORMAT = 'AUTO' NULL_IF = ('');

-- 3.2.4
--URL = s3://snowflake-workshop-lab/citibike_trips
list @citibike_trips;

--4.2.2

copy into trips from @citibike_trips
file_format=CSV;


-- 4.2.4

truncate table trips;

-- 4.2.6

copy into trips from @citibike_trips
file_format=CSV;

-- 5.1.2

select * from trips limit 20;

-- 5.1.3

select date_trunc('hour', starttime) as "date",
count(*) as "num trips",
avg(tripduration)/60 as "avg duration (mins)", 
avg(haversine(start_station_latitude, start_station_longitude, end_station_latitude, end_station_longitude)) as "avg distance (km)" 
from trips
group by 1 order by 1;

-- 5.1.4

select date_trunc('hour', starttime) as "date",
count(*) as "num trips",
avg(tripduration)/60 as "avg duration (mins)", 
avg(haversine(start_station_latitude, start_station_longitude, end_station_latitude, end_station_longitude)) as "avg distance (km)" 
from trips
group by 1 order by 1;

-- 5.1.5

select
    dayname(starttime) as "day of week",
    count(*) as "num trips"
from trips
group by 1 order by 2 desc;

-- 5.2.1

create table trips_dev clone trips;

select count(*) from TRIPS;
select count(*) from TRIPS_DEV;

-- 6.1.1

create database WEATHER;

-- 6.1.3

USE DATABASE WEATHER;
USE WAREHOUSE COMPUTE_WH;
USE SCHEMA PUBLIC;

-- 6.1.4

CREATE OR REPLACE TABLE JSON_WEATHER_DATA (v variant);

-- 6.2.3

CREATE STAGE "WEATHER"."PUBLIC".nyc_weather URL = 's3://snowflake-workshop-lab/weather-nyc';

-- 6.2.4 

list @nyc_weather;

-- 6.3.2

COPY INTO json_weather_data FROM @nyc_weather 
FILE_FORMAT = (TYPE=JSON);

-- 6.3.3

SELECT * FROM json_weather_data LIMIT 10;

-- 6.4.1

select
  v:time::timestamp as observation_time,
  v:city.id::int as city_id,
  v:city.name::string as city_name,
  v:city.country::string as country,
  v:city.coord.lat::float as city_lat,
  v:city.coord.lon::float as city_lon,
  v:clouds.all::int as clouds,
  (v:main.temp::float)-273.15 as temp_avg,
  (v:main.temp_min::float)-273.15 as temp_min,
  (v:main.temp_max::float)-273.15 as temp_max,
  v:weather[0].main::string as weather,
  v:weather[0].description::string as weather_desc,
  v:weather[0].icon::string as weather_icon,
  v:wind.deg::float as wind_dir,
  v:wind.speed::float as wind_speed
from json_weather_data
where city_id = 5128638 limit 20;

-- 6.5.1

create or replace view JSON_WEATHER_DATA_VIEW as
select
  v:time::timestamp as observation_time,
  v:city.id::int as city_id,
  v:city.name::string as city_name,
  v:city.country::string as country,
  v:city.coord.lat::float as city_lat,
  v:city.coord.lon::float as city_lon,
  v:clouds.all::int as clouds,
  (v:main.temp::float)-273.15 as temp_avg,
  (v:main.temp_min::float)-273.15 as temp_min,
  (v:main.temp_max::float)-273.15 as temp_max,
  v:weather[0].main::string as weather,
  v:weather[0].description::string as weather_desc,
  v:weather[0].icon::string as weather_icon,
  v:wind.deg::float as wind_dir,
  v:wind.speed::float as wind_speed
from json_weather_data
where city_id = 5128638;

-- 6.5.3

select * from JSON_WEATHER_DATA_VIEW
where date_trunc('month',observation_time) = '2018-01-01' 
limit 20;

-- 6.6.1

create or replace view trips_weather_view as
select * from citibike.public.trips 
left outer join json_weather_data_view
on date_trunc('hour', observation_time) = date_trunc('hour', starttime);

-- 6.6.3

select weather as conditions,
count(*) as num_trips
from trips_weather_view
where conditions is not null
group by 1 order by 2 desc;

-- 7.1.1

drop table json_weather_data;

-- 7.1.2

SELECT * FROM json_weather_data LIMIT 10;

-- 7.1.3

undrop table json_weather_data;

-- 7.2.1

USE DATABASE CITIBIKE;
update trips set start_station_name = 'oops';

-- 7.2.2

select 
start_station_name as "station",
count(*) as "rides"
from trips
group by 1
order by 2 desc
limit 20;

-- 7.2.3

set query_id = 
(select query_id from 
table(information_schema.query_history_by_session (result_limit=>5)) 
where query_text like 'update%' order by start_time limit 1);

create or replace table trips as 
		(select * from trips before (statement => $query_id));
        
-- 7.2.4

select 
start_station_name as "station",
count(*) as "rides"
from trips
group by 1
order by 2 desc
limit 20;

-- 8.1.1
USE ROLE ACCOUNTADMIN;

-- 8.1.3

CREATE ROLE JUNIOR_DBA;
GRANT ROLE JUNIOR_DBA TO USER xxxxxxx; -- change YOUR_USER_NAME_GOES_HERE


-- 8.1.4

USE ROLE JUNIOR_DBA;

-- 8.1.6

use role accountadmin;
grant usage on database citibike to role junior_dba;
grant usage on database weather to role junior_dba;

-- 8.1.7

USE ROLE junior_dba;
